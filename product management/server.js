import express from "express";
import dotEnv from "dotenv";

class app {
	constructor() {
		dotEnv.config();

		db.dbConnect()
			.then(postgre => {
				return this.configureApp(postgre);
			})
			.then(app => {
				return this.startServer(app);
			});
	}

	configureApp = (postgre) => {
		return new Promise((resolve, reject) => {
			const app = express();
			app.set("base", process.env.API_PREFIX);
			app.use(cors());
			app.use(nocache());
			app.use(expressUpload());
			app.use(express.json());
			app.use(express.urlencoded({ extended: true }));
			app.use(express.static(path.join(__dirname, "public")));
			app.use(`/${process.env.API_PREFIX}`, routes);

			// 404
			app.use((req, res, next) => {
				return res.status(404).send({
					error: `Not found: ${req.url}`
				});
			});
			// 500
			app.use((err, req, res, next) => {
				console.log(err); // write to pm2 logs
				const statusCode = err.status || 500;
				const { message, ...errors } = err;
				const error = Object.keys(errors).length ?
					Object.values(errors) :
					{ error: message };
				return res.status(statusCode).send(error);
			});

			global.db = postgre;
			global.rootDir = path.resolve(__dirname);
			resolve(app);
		});
	}

	startServer = (app) => {
		return new Promise((resolve, reject) => {
			// set port
			const port = process.env.PORT;
			app.set("port", port);
			// create HTTP server
			const server = http.createServer(app);
			// attach handler
			server.on("listening", () => {
				const bind = server.address().port;
				console.log("Gami API listening on " + `http://localhost:${bind}/${process.env.API_PREFIX}`);
			});
			// listen on provided port, on all network interfaces
			server.listen(port);

			resolve(server);
		});
	}
}

export default new app();