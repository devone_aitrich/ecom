'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable("medias", {
			id: {
				autoIncrement: true,
				primaryKey: true,
				allowNull: false,
				type: Sequelize.INTEGER,
			},
			media_name: {
				allowNull: false,
				type: Sequelize.TEXT
			},
			media_url: {
				allowNull: false,
				type: Sequelize.TEXT
			},
			created_by: {
				allowNull: true,
				type: Sequelize.INTEGER
			},
			updated_by: {
				allowNull: true,
				type: Sequelize.INTEGER
			},
			created_at: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updated_at: {
				allowNull: false,
				type: Sequelize.DATE
			},
			deleted_at: {
				allowNull: true,
				type: Sequelize.DATE
			}
		});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
